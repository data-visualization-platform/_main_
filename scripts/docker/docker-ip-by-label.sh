#!/bin/bash

#1 = label string
c="docker ps -aq --filter 'label=$1' --filter 'status=running'"
id=$(eval "$c")
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' "$id"