#!/bin/bash

echo "Running image: $2"
# $3 -p 8080:8080 PORT
# $4 parameters

# Use gitlab.registry
if $1; then
    c="docker run $3 $4 registry.gitlab.com/data-visualization-platform/$2:latest"
else
    c="docker run $3 $4 $2"
fi

echo "$c"

{
  eval "$c"
  # Keep Server Running...
} || {
  echo ""
  /bin/echo -e "\e[31m========================"
  /bin/echo -e "\e[31mERROR Docker run failed!"
  /bin/echo -e "\e[31m========================"
  /bin/echo -e "\e[39mDocker_Op_Completed"
  exit 1
}
