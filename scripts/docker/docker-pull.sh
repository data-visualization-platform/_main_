#!/bin/bash

echo "Pulling image: $2"

# Use gitlab.registry
if $1; then
    c="docker pull registry.gitlab.com/data-visualization-platform/$2:latest"
else
    c="docker pull $2"
fi

echo "$c"

{
  eval "$c" && echo "Successfully pulled image: $2 - Docker_Op_Completed"
} || {
  echo ""
  /bin/echo -e "\e[31m========================"
  /bin/echo -e "\e[31mERROR Docker run failed!"
  /bin/echo -e "\e[31m========================"
  /bin/echo -e "\e[39mDocker_Op_Completed"
  exit 1
}


