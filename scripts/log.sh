#!/bin/bash

echo -e "\e[33m======================"
echo -e "\e[33m| Docker Information |"
echo -e "\e[33m======================"
echo -e "\e[39m"

docker ps -a

echo ""
echo -e "\e[33m======================"
echo -e "\e[33m|  List Open Ports   |"
echo -e "\e[33m======================"
echo -e "\e[39m"

lsof -i -P -n | grep LISTEN