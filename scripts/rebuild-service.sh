#!/bin/bash


BROKER_ADDRESSES=$(./kafka/broker-ip.sh "$2")
/bin/echo -e "\e[90m$1" # MS FOLDER LOCATION
/bin/echo -e "\e[90m$2" # IPADDRESS
/bin/echo -e "\e[90m$3" # FRONTEND PORT
/bin/echo -e "\e[90m$BROKER_ADDRESSES"
/bin/echo -e "\e[39m"
# $4 IGNORE KAFKA
# $5 HOST

if [ -z "$4" ]; then
  if [ -z "$BROKER_ADDRESSES" ]
  then
      /bin/echo -e "\e[91mCould not find any broker ip addresses. Try starting Kafka First.\e[39m"
      exit 1
  fi
fi

cd ../../"$1" || exit 1

echo "Rebuilding Service: $1"
/bin/echo -e "\e[33mBe patient. Pulling the remote image might take some while.\e[39m"

HOST=$5 KAFKA_ADVERTISED_HOST_NAME=$2 PORT=$3 PROXY_PORT=2999 CHOKIDAR_USEPOLLING=true BROKERS_IP_ADDRESSES=$BROKER_ADDRESSES docker-compose build --no-cache

echo "Successfully Rebuild Service: $1"