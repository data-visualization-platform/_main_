#!/bin/bash

# Read Project List (1st parameter)
projectArr=("$@")
IFS=' ' read -a projects <<< "${projectArr}"

echo ""
echo ""
echo '__/\\\\\\\\\\\\_____/\\\________/\\\__/\\\\\\\\\\\\\___
 _\/\\\////////\\\__\/\\\_______\/\\\_\/\\\/////////\\\_       
  _\/\\\______\//\\\_\//\\\______/\\\__\/\\\_______\/\\\_      
   _\/\\\_______\/\\\__\//\\\____/\\\___\/\\\\\\\\\\\\\/__     
    _\/\\\_______\/\\\___\//\\\__/\\\____\/\\\/////////____    
     _\/\\\_______\/\\\____\//\\\/\\\_____\/\\\_____________   
      _\/\\\_______/\\\______\//\\\\\______\/\\\_____________  
       _\/\\\\\\\\\\\\/________\//\\\_______\/\\\_____________ 
        _\////////////___________\///________\///______________';
sleep 2
echo ''

echo 'Install docker and docker-compose using the offical docker documentation';
echo 'Login in first with docker login into a terminal before starting this script';


# Ensure Docker and docker compose is installed is installed
if ! [ -x "$(command -v docker)" ]; then
    echo "Command docker could not be found. Install docker please!"
    exit 1
fi

if ! [ -x "$(command -v docker-compose)" ]; then
    echo "Command docker-compose could not be found. Install docker please!"
    exit 1
fi

if ! [ -x "$(command -v docker-machine)" ]; then
    echo "Command docker-machine could not be found. Run: sudo snap install docker!"
    exit 1
fi

# if ! [[ $(docker-machine active) = default ]]; then
#     echo "The 'default' docker-machine is not active!"
#     exit 1
# fi

# Export Docker host
export DOCKERHOST=$(ifconfig | grep -E "([0-9]{1,3}\.){3}[0-9]{1,3}" | grep -v 127.0.0.1 | awk '{ print $2 }' | cut -f2 -d: | head -n1)

# Error out if project was not pulled, yet
for i in "${projects[@]}"
do
    if ! [ -d "../$i" ]
    then
        echo "Cannot find: $i"
        exit 1
#    else
#        git --git-dir="../$i/.git" fetch --dry-run
    fi
done

# Set permission for current user on docker folder
chown "$USER":"$USER" /home/"$USER"/.docker -R
chmod g+rwx "/home/$USER/.docker" -R

echo ""
echo ""
echo -e "\e[96mStarting local docker registry\e[0m"
docker-compose up -d
echo -e "\e[0m" # reset colors

echo ""
echo -e "\e[92m====================================================="
echo -e "\e[92m|             SUCCESSFULLY CONFIGURED               |"
echo -e "\e[92m====================================================="
echo -e "\e[0m" # reset colors
echo ""