#!/bin/bash

minikube start --memory=4096 --cpus=4 --disk-size=40000mb