#!/bin/bash

docker container stop $(docker container ls -q --filter name=kafka-middle-layer*)

echo 'Stopped Kafka'