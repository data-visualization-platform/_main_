#!/bin/bash

host=$1

if [[ -n "$host" ]]; then
	CONTAINERS=$(docker ps | grep 9092 | awk '{print $1}')
	BROKERS=$(for CONTAINER in ${CONTAINERS}; do docker port "$CONTAINER" 9092 | sed -e "s/0.0.0.0:/$host:/g"; done)
	BROKERS_IP_ADDRESSES=$(echo ${BROKERS[*]}) | tr ' ' ','
	echo $(echo ${BROKERS[*]}) | tr ' ' ','
else
    /bin/echo -e "\e[91mNo host ip address passed as an argument!\e[39m"
fi
