#!/bin/bash

echo "Creating Kafka Testing Consumer"

docker exec -t kafka-middle-layer_kafka_1 kafka-console-consumer.sh --bootstrap-server :9092 --group trincom --topic Upload_Kafka_Topic

echo "Waiting for messages"