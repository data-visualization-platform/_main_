#!/bin/bash

cd ../../../kafka-middle-layer || exit
echo ''
echo "$PWD"

echo "Starting Kafka on host: $1"

#gnome-terminal -e "bash -c 'IPADDRESS=$1 docker-compose up'"
IPADDRESS=$1 docker-compose up -d
IPADDRESS=$1 docker-compose scale kafka=3
docker exec -t kafka-middle-layer_kafka_1 kafka-topics.sh --bootstrap-server :9092 --create --topic Upload_Kafka_Topic --partitions 3 --replication-factor 1
docker exec -t kafka-middle-layer_kafka_1 kafka-topics.sh --bootstrap-server :9092 --describe --topic Upload_Kafka_Topic

echo ""
echo "====================================================="
echo "|           KAFKA SUCCESSFULLY STARTED               |"
echo "====================================================="
echo ""

echo 'Kafka Running'